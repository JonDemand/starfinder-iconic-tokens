NOTE - As of Version 11 of Foundry and 0.25 of the Starfinder system, this module is no longer required, as new artwork and tokens are included in the base system,  including previously unavailable artwork for Barsala, Velloro and Zemir :))

A collection of Pog Style tokens for the Iconic Characters included in the Starfinder Game system on Foundry - sfrpg

Background and Token Ring through Adobe Stock and shutterstock with commercial licenses and put together my myself

This module uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy (paizo.com/communityuse). We are expressly prohibited from charging you to use or access this content. This module is not published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit paizo.com.
